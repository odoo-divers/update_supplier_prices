{
    'name' : 'Update Supplier Prices',
    'version' : '0.2',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'purchase',
    ],
    'data' : [
    ],

    'installable' : True,
    'application' : False,
}
