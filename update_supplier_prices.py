# -*- coding: utf-8 -*-
##############################################################################
#
#    update_supplier_prices for Odoo/OpenERP
#    Copyright (c) 2015 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api

class account_invoice(models.Model):
    _name = "account.invoice"
    _inherit = "account.invoice"

    #Sucharge de la methode invoice_validate pour mettre à jour les prix fournisseurs
    def invoice_validate(self):
        if self.type == "in_invoice":
            for line in self.invoice_line:
                if line.product_id.product_tmpl_id.id:
                    supplierinfo_ids = self.env['product.supplierinfo'].search([('product_tmpl_id', '=', line.product_id.product_tmpl_id.id), ('name', '=', line.partner_id.id)])
                    if len(supplierinfo_ids) > 0:
                        for supllierinfo_id in supplierinfo_ids:
                            pricelists = self.env['pricelist.partnerinfo'].search([('suppinfo_id', '=', supllierinfo_id.id)])
                            for pricelist in pricelists:
                                if pricelist.min_quantity == 1:
                                    pricelist.price = line.price_unit
                    else:
                        suppinfo = self.env['product.supplierinfo'].create({
                            'name': line.partner_id.id,
                            'product_tmpl_id': line.product_id.product_tmpl_id.id,
                        })
                        if suppinfo:
                            self.env['pricelist.partnerinfo'].create({
                                'price': line.price_unit,
                                'suppinfo_id': suppinfo.id,
                                'min_quantity': 1,
                            })

        return self.write({'state': 'open'})
